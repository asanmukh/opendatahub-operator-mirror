---
# tasks file for AI CoE JupyterHub
################################################################################
# NOTEBOOKS BUILD CONFIGS AND IMAGESTREAMS
################################################################################
- name: Create notebook ImageStreams
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item.file) }}"
  when: deploy_all_notebooks == True
  with_items:
    - file: jupyter-notebook-images/imagestream.json.j2
      image: s2i-minimal-notebook
      tag: "3.6"
    - file: jupyter-notebook-images/imagestream.json.j2
      image: s2i-scipy-notebook
      tag: "3.6"
    - file: jupyter-notebook-images/imagestream.json.j2
      image: s2i-tensorflow-notebook
      tag: "3.6"
    - file: jupyter-notebook-images/imagestream.json.j2
      image: nbviewer
      tag: "latest"

- name: Create ALL notebook BuildConfigs
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item) }}"
  when: deploy_all_notebooks == True and not registry
  with_items:
    - jupyter-notebook-images/minimal-notebook-buildconfig.json.j2
    - jupyter-notebook-images/scipy-notebook-buildconfig.json.j2
    - jupyter-notebook-images/tensorflow-notebook-buildconfig.json.j2
    - nbviewer-images/nbviewer-buildconfig.json.j2

- name: Create spark notebook ImageStreams
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item.file) }}"
  when: deploy_all_notebooks == False
  with_items:
    - file: jupyter-notebook-images/imagestream.json.j2
      image: s2i-spark-minimal-notebook
      tag: "3.6"
    - file: jupyter-notebook-images/imagestream.json.j2
      image: nbviewer
      tag: "latest"

- name: Create spark notebook BuildConfigs
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item) }}"
  when: deploy_all_notebooks == False and not registry
  with_items:
    - jupyter-notebook-images-spark/spark-minimal-notebook-buildconfig.json.j2
    - nbviewer-images/nbviewer-buildconfig.json.j2

- name: Create ALL spark notebook ImageStreams
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item.file) }}"
  when: deploy_all_notebooks == True
  with_items:
    - file: jupyter-notebook-images/imagestream.json.j2
      image: s2i-spark-minimal-notebook
      tag: "3.6"
    - file: jupyter-notebook-images/imagestream.json.j2
      image: s2i-spark-scipy-notebook
      tag: "3.6"

- name: Create ALL spark notebook BuildConfigs
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item) }}"
  when: deploy_all_notebooks == True and not registry
  with_items:
    - jupyter-notebook-images-spark/spark-minimal-notebook-buildconfig.json.j2
    - jupyter-notebook-images-spark/spark-scipy-notebook-buildconfig.json.j2

- name: Add CUDA builds
  when: deploy_cuda_notebooks == True
  block:
    - name: Configure CUDA build chian
      set_fact:
        CUDA_VERSION: "10.0"
        S2I_PYTHON_VERSION: "3.6"
        OS_VERSION: "ubi7"     
    - name: Process CUDA build chain
      command: oc process -o yaml \
                          --param=S2I_IMAGE=registry.access.redhat.com/ubi7/s2i-base:latest  \
                          --param=SOURCE_REPOSITORY_CONTEXT_DIR={{ CUDA_VERSION }} \
                          --param=S2I_PYTHON_VERSION={{ S2I_PYTHON_VERSION }} \
                          --param=APPLICATION_NAME={{ CUDA_VERSION }}-cuda-chain-{{ OS_VERSION }}  \
                          --param=APPLICATION_NAME_1={{ CUDA_VERSION }}-base-{{ OS_VERSION }}  \
                          --param=APPLICATION_NAME_2={{ CUDA_VERSION }}-runtime-{{ OS_VERSION }}   \
                          --param=APPLICATION_NAME_3={{ CUDA_VERSION }}-cudnn7-runtime-{{ OS_VERSION }}   \
                          --param=APPLICATION_NAME_4={{ CUDA_VERSION }}-devel-{{ OS_VERSION }}  \
                          --param=APPLICATION_NAME_5={{ CUDA_VERSION }}-cudnn7-devel-{{ OS_VERSION }} \
                          --param=APPLICATION_NAME_6=python-36-{{ OS_VERSION }} \
                          -f -
      args:
        stdin: "{{ lookup('template', item, convert_data=False) | string }}"
      register: processed_template
      with_items:
        - jupyter-gpu-build-chain/cuda-build-chain.json.j2
    - name: Apply CUDA build chain
      k8s:
        state: "{{ state }}"
        namespace: "{{ meta.namespace }}"
        definition: "{{ item.stdout | from_yaml }}"
      loop: "{{ processed_template.results }}"
    - name: Create JupyterHub CUDA enabled notebook images
      openshift_raw:
        state: "{{ state }}"
        definition: "{{ lookup('template', item.file) }}"
      with_items:
        - file: jupyter-notebook-images/imagestream.json.j2
          image: s2i-minimal-notebook-gpu
          tag: "3.6"
        - file: jupyter-notebook-images/imagestream.json.j2
          image: s2i-tensorflow-gpu-base
          tag: "3.6"
        - file: jupyter-notebook-images/imagestream.json.j2
          image: s2i-tensorflow-notebook-gpu
          tag: "3.6"
    - name: Create JupyterHub CUDA enabled notebook BuildConfigs
      openshift_raw:
        state: "{{ state }}"
        definition: "{{ lookup('template', item)}}"
      with_items:
        - jupyter-gpu-build-chain/minimal-gpu-buildconfig.json.j2
        - jupyter-gpu-build-chain/tensorflow-gpu-base-buildconfig.json.j2
        - jupyter-gpu-build-chain/tensorflow-gpu-buildconfig.json.j2


################################################################################
# JUPYTERHUB DEPLOYMENTS
################################################################################
- name: Create JupyterHub ImageStreams
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item.file)}}"
  with_items:
    - file: jupyter-notebook-images/imagestream.json.j2
      image: jupyterhub
      tag: "{{ jupyterhub_base_tag }}"
    - file: jupyter-notebook-images/imagestream.json.j2
      image: jupyterhub-img
      tag: "{{ jupyterhub_img_tag }}"

- name: Create JupyterHub BuildConfigs
  openshift_raw:
    state: "{{ state }}"
    definition: "{{ lookup('template', item)}}"
  when: not registry
  with_items:
    - jupyterhub-deployer/jupyterhub-buildconfig.json.j2
    - jupyterhub-deployer/jupyterhub-dh-buildconfig.json.j2

- name: Create JupyterHub RBAC
  k8s:
    state: "{{ state }}"
    definition: "{{ lookup('template', item)}}"
  with_items:
    - jupyterhub-deployer/serviceaccount.json.j2
    - jupyterhub-deployer/role.json.j2
    - jupyterhub-deployer/rolebinding.json.j2

- name: Create the jupyterhub_config ConfigMap
  k8s:
    state: "{{ state }}"
    definition: "{{ lookup('template', item)}}"
  with_items:
    - jupyterhub-deployer/configmap.json.j2

- name: Deploy the Jupyterhub database
  k8s:
    state: "{{ state }}"
    definition: "{{ lookup('template', item)}}"
  with_items:
    - jupyterhub-deployer/jupyterhub-db-pv.json.j2
    - jupyterhub-deployer/jupyterhub-db-dc.json.j2
    - jupyterhub-deployer/jupyterhub-db-service.json.j2

- name: Deploy Jupyterhub
  k8s:
    state: "{{ state }}"
    definition: "{{ lookup('template', item)}}"
  with_items:
    - jupyterhub-deployer/jupyterhub-dc.json.j2
    - jupyterhub-deployer/jupyterhub-service.json.j2
    - jupyterhub-deployer/route.json.j2
    - jupyterhub-deployer/jupyterhub-singleuser-profiles-configmap.yaml.j2

# Deploy the template that jupyterhub-singleuser-profiles references to spawn spark clusters per user notebook pod
- name: Add spark-operator configmap template
  k8s:
    state: "{{ state }}"
    definition: "{{ lookup('template', item)}}"
  with_items:
    - jupyterhub-deployer/jupyterhub-spark-operator-configmap.yaml.j2

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="deploying-kafka"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Deploying Kafka Setup
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

This procedure describes how to enable (and disable) Kafka for the Open Data Hub

.Prerequisites

* A terminal shell with the OpenShift client command `oc` availabe.
* A copy of the files in this repository available in your terminal shell.
* The ability to run the operator as outlined at link:manual-installation.adoc[manual-installation]
* Be able to deploy Kafka Cluster operator as cluster-admin

( *Note:* The link:strimzi.io[strimzi] operator we use to deploy Kafka requires to be deployed with a user with link:https://strimzi.io/docs/latest/#why_do_i_need_cluster_admin_privileges_to_install_strimzi[cluster-admin])


.Procedure

. Change directory to your copy of the repository.
. Login as cluster-admin
. Run the manual-installation steps 1 - 5 as outlined in this link:manual-installation.adoc[manual-installation]
. Note: To interact with the Kafka cluster each allowed user needs an additional role binding of the type `StrimziAdmin`. These users can be set in `deploy/kafka/vars/vars.yaml` as a list. The default are user `admin` and the `opendatahub-operator` service-account
+
....
  kafka_admins:
    - <user1>
    - <user2>
....
+
Note: The opendatahub-operator service-account will be able to deploy and manage kafka in the namespace its deployed in.
. Deploy Kafka (Strimzi) cluster operator
+
....
cd deploy/kafka/
pipenv install
pipenv run ansible-playbook deploy_kafka_operator.yaml \
  -e kubeconfig=$HOME/.kube/config \
  -e NAMESPACE=<namespace in which to deploy kafka operator>

cd ../../
....
. Login to the cluster as a regular user with the `StrimziAdmin` role binding or with the `opendatahub-operator` service-account as outlined in steps 1-2 link:run-operator-remotely.adoc[here]
. Copy the default Open Data Hub custom resource manifest, copying this file will allow you to make changes for your environment without disturbing the original file
+
....
# Make sure you're in the root folder of opendatahub-operator
cp deploy/crds/opendatahub_v1alpha1_opendatahub_cr.yaml my_environment_cr.yaml
....

. Open `my_environment_cr.yaml` in the editor of your choice
. Setting `odh_deploy` to `true` or `false` will either enable or disable Kafka deployment. Edit the following lines in the file to do so
+
....
  kafka:
    odh_deploy: true
....
. Deploy the Open Data Hub using the custom resource manifest for your
  environment by entering the following command.
+
....
oc apply -f my_environment_cr.yaml
....

. This will deploy a default Kafka cluster with the name `odh-message-bus-kafka` with 3 Kafka brokers and 3 zookeeper brokers with a kafka-admin user called `admin` and monitoring enabled.
. To deploy a more customized kafka cluster apply the kafka custom resource as per the documentation link:https://strimzi.io/docs/latest/#con-custom-resources-example-str[here]
. To get better insight into the Kafka cluster you can enable Monitoring in ODH by setting `monitoring` to `true` in the ODH custom resource as per the document link:deploying-monitoring.adoc[here]
+
....
monitoring:
  odh_deploy: true
....
+
This will add the default dashboard for Kafka which includes graphs for key metrics like Brokers Available, Message Rate, Available Partitions, Network Metrics, etc. This will give you an overview of the kafka cluster.

//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

.Additional resources

* More information about *Apache Kafka* can be found link:https://Kafka.apache.org/[here].
* More information about *Strimzi Operator* can be found link:https://strimzi.io[here].

**NOTE:** We are currently using `strimzi-operator v0.11.1` with `Apache Kafka v2.1.0` since most libraries support this version as of now.
To perform a strimzi version upgrade replace the cluster-object files with files in link:https://github.com/strimzi/strimzi-Kafka-operator/releases/download/0.12.2/strimzi-0.12.2.tar.gz[here]

.Steps to update strimzi version and Kafka version:

* Replace files in `opendatahub-operator/deploy/kafka/operator-objects/` with files in `<strimzi-download>/install/cluster-operator`
*  Replace the namespace with the namespace-variable
....
sed -i 's/namespace: .*/namespace: "{{ NAMESPACE }}"/' opendatahub-operator/roles/Kafka/templates/cluster-objects/*RoleBinding*.yaml
....
* Then you can apply the updated version of the Kafka custom resource. You can find documentation on it link:https://strimzi.io/docs/latest/#con-custom-resources-example-str[here]
* Be careful when upgrading Kafka versions not all versions are backwards compatible and some might require upgrading the strimzi version first and then upgrading the Kafka and log-message-format versions in steps. For more information click link:https://strimzi.io/docs/latest/#assembly-upgrade-str[here]

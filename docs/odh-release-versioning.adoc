[id="odh-versioning"]
= Open Data Hub Version Release

Open Data Hub uses link:https://semver.org/[Semantic Versioning] for all release numbers in the format of X.Y.Z.

* X := Major updates to the Open Data Hub operator architecture
* Y := Addition of new components and major enhancements to existing components
* Z := Patches and bug fixes for existing components

Upon release of a new opendatahub-operator image, we will create a git tag in this repository and build a new image based off of that tag in our link:https://quay.io/opendatahub/opendatahub-operator[quay.io] repository.

* *X.Y.Z* tags will be created for each tagged release
* *X.Y* will point to the latest X.Y.Z release. This tag will ensure that you always get the latest bug fixes inside of a specific version
* *latest* will always point to the latest build of the Open Data Hub operator based on the master branch

As part of the release process, the Open Data Hub operator default link:deploy/operator.yaml[deployment] will be updated to point to the latest tagged release.

Notification of a version release will be sent out to to the opendatahub.io announcements link:https://opendatahub.io/community.html[mailing list] with a description of the major updates included in the release.
